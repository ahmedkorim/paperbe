const bcrypt = require("bcrypt");
const crypto = require("crypto");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

const userSchema = new mongoose.Schema(
  {
    email: { type: String, unique: true },
    password: String,
    passwordResetToken: String,
    passwordResetExpires: Date,
    tokens: Array,

    profile: {
      name: String,
      gender: String,
      website: String,
      picture: String
    },

    roles: [
      String // A user can be both owner and a customer
    ]
  },
  { timestamps: true }
);

/**
 * Password hash middleware.
 */
userSchema.pre("save", function save(next) {
  const user = this;
  if (!user.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    });
  });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(
  candidatePassword,
  cb
) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

/**
 *  Generating an authToken with jwt
 */
userSchema.methods.generateAuthToken = function() {
  const token = jwt.sign(
    {
      _id: this._id,
      email: this.email,
      roles: this.roles,
      profile: this.profile
    },
    process.env.JWT_SECRET, //get the private key from the config file -> environment variable
    {
      expiresIn: "7d" // expires in 1 week
    }
  ); 
  return token;
};

/**
 *  Getting a token
 */
userSchema.methods.toAuthJSON = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateAuthToken(),
    profile: this.profile
  };
};

const User = mongoose.model("User", userSchema);

module.exports = User;
