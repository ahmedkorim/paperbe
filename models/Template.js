const mongoose = require("mongoose");
var Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId,
    Mixed = Schema.Types.Mixed;

const templateSchema = new mongoose.Schema(
  {
    user_id: String,
    data_type: String,
    id:{type:String, unique:true, dropDups: true }
  }, // Schemless schema is better for now
  { timestamps: true , strict: false  }
);

const Template = mongoose.model("Template", templateSchema);

module.exports = Template;