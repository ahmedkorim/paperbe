const { promisify } = require('util');
const crypto = require('crypto');
const passport = require('passport');
const _ = require('lodash');
const User = require('../models/User');
const routes = require('express').Router();

const randomBytesAsync = promisify(crypto.randomBytes);

/**
 * GET /login
 * Login page.
 */
routes.get('/', (req, res) => {

    if (req.user) {
    return res.status(200).json({
        message: 'Hello u r  logged in'
      });
  }
  // Go to Landing Page
  res.status(403).json({
    message: 'Hello r not logged in'
  });
})


// Entering dashboard
routes.get('/account/:email', (req, res) => {
    if(req.user){
        User.findOne({email:req.params.email}).then((user)=>{
            if(user){
                return res.status(200).json(user.toAuthJSON());
            }else {
                return res.status(404).json({message:["user NOT FOUND"]})
            }
        })
    }else {
        // Go to Landing Page
        res.status(493).send({
            title: 'Hello r not logged in'
        });
    }
  
})

module.exports = routes;

