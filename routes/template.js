const routes = require( 'express' ).Router();
const User = require( '../models/User' );
const Template = require( '../models/Template' );
var ObjectId = require( 'mongoose' ).Types.ObjectId;


/**
 *  Get all templates
 */
routes.get( '/all', ( req, res ) => {
  if ( req.user ) {
    Template.find(
      {
        user_id: new ObjectId( req.user._id ),
        data_type: "template"
      },
      ( err, templates ) => {
        if ( err ) {
          res.status( 404 ).json( { message: "Error getting templates" } )
        } else {


          // Note: returned templates will be in form of array
          if ( templates && templates.length > 0 ) {
            res.status( 200 ).json(
              {
                message: "Successfully retrieved all the user's templates",
                data: templates
              } )
          } else {
            res.status( 200 ).json(
              {
                message: "No Templates found for this user",
                data: []
              } )
          }
        }
      } );
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )


/**
 *  Save a new template
 */
routes.post( '/save', ( req, res ) => {
  if ( req.user ) {
    if ( req.body.template ) {

      const template = new Template( req.body.template );
      template.user_id = req.user._id;
      template.data_type = "template";

      template.save( err => {
        if ( err ) {
          console.log( err )
          if ( err.code == "11000" ) {
            res.status( 422 ).json( { message: "Template id already exists!! " } )

          } else {
            res.status( 404 ).json( { message: "Error saving template" } )
          }
        } else {
          res.status( 200 ).json( { message: "Template Saved Successfully", data: { id: template._id.toString() } } )

        }
      } )
    } else {
      res.status( 404 ).send( {
        message: "no template data were sent"
      } )
    }
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )


/**
 *  Get a specific  template
 */
routes.get( '/get/:template_id', ( req, res ) => {
  if ( req.user ) {
    Template.findOne(
      {
        _id: new ObjectId( req.params.template_id ),
        user_id: new ObjectId( req.user._id ),
        data_type: "template"
      },
      ( err, template ) => {
        if ( err ) {
          res.status( 404 ).json( { message: "Error getting templates" } )
        } else {

          // Note: returned templates will be in form of document
          if ( template ) {
            res.status( 200 ).json(
              {
                message: "Successfully retrieved the template",
                data: template
              } )
          } else {
            res.status( 404 ).json(
              {
                message: "No Templates found for this user",

              } )
          }
        }
      } );
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

/**
 *  Update an existing template
 */
routes.put( '/update/:template_id', ( req, res ) => {
  if ( req.user ) {

    if ( req.body.template ) {
      Template.updateOne( { _id: ObjectId( req.params.template_id ) }, req.body.template,
        ( err, doc ) => {

          if ( err ) {
            res.status( 404 ).send( {
              message: "An error occured updating the template please try again"
            } )
          } else if ( doc.nModified == 0 ) {
            res.status( 404 ).send( {
              message: "No templete have been updated"
            } )
          } else {
            //
            res.status( 200 ).send(
              { message: "Template Updated Successfully" }
            )
          }
        } )
    } else {
      res.status( 404 ).send( {
        message: "no template data were sent"
      } )
    }
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

/**
 * Delete one Template
 */
routes.delete( "/delete/:template_id", ( req, res ) => {
  if ( req.user ) {

    Template.deleteOne( { _id: ObjectId( req.params.template_id ) },
      ( err, doc ) => {

        if ( err ) {
          res.status( 404 ).send( {
            message: "An error occured deleting the template please try again"
          } )
        } else if ( doc.deletedCount == 0 ) {
          res.status( 404 ).send( {
            message: "No document have been deleted"
          } )
        } else {
          // Successfully Deleted
          res.status( 200 ).send(
            { message: "Template Deleted Successfully" }
          )
        }

      } )

  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

module.exports = routes;
