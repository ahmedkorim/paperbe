const { Router } = require( 'express' );
const puppeteer = require( 'puppeteer' )
const exportPdfRouter = Router();
const sendTo = require( '../utils/nodeMailer' );

exportPdfRouter.post( '/(:mailTo)?', async ( req, res ) => {
  const { content, message, mailFrom } = req.body;
  const { mailTo } = req.params;

  var contentType = req.headers[ 'content-type' ];

  if ( !contentType || contentType.indexOf( 'application/json' ) !== 0 || content.trim() === '' ) {
    return res
      .status( 400 )
      .send( 'Bad request' )
  }
  try {
    const browser = await puppeteer.launch();
    const page = await browser.newPage()
    await page.setContent( content )
    const pdf = await page.pdf( {
      format: 'A4', margin: {
        top: 50,
        bottom: 50,
        left: 20,
        right: 20,
      },
      displayHeaderFooter: true,
      headerTemplate: '<div id="header-template"></div>',
      footerTemplate: '<div id="footer-template" style="font-size:10px !important; color:#808080; padding-left:10px"><span>PaprWork Beta</span> - </span><span class="pageNumber"></span>/<span class="totalPages"></span></div>',
    } );
    await browser.close()

    if ( mailTo ) {
      z
      // Validate
      if ( ( /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i ).test( mailTo ) ) {
        // mail to login

        await sendTo( mailFrom, mailTo, pdf, message, content )

      } else {
        res
          .status( 400 )
          .send( { message: [ 'Please Enter a valid Email' ] } )
      }
    } else {

      res
        .set( 'Content-Type', 'application/pdf' )
        .status( 201 )
        // !todo refactor to a stream
        .send( `data:application/pdf;base64,${ pdf.toString( 'base64' ) }` )
    }
  } catch ( e ) {
    console.log( e );
    res
      .status( 500 )
      .send( e )
  }

} )


module.exports = exportPdfRouter;

