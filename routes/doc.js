const routes = require( 'express' ).Router();
const User = require( '../models/User' );
const Template = require( '../models/Template' );
var ObjectId = require( 'mongoose' ).Types.ObjectId;


/**
 *  Get all docs
 */
routes.get( '/all', ( req, res ) => {
  if ( req.user ) {
    Template.find(
      {
        user_id: new ObjectId( req.user._id ),
        data_type: "doc"
      },
      ( err, docs ) => {
        if ( err ) {
          res.status( 404 ).json( { message: "Error getting docs" } )
        } else {


          // Note: returned docs will be in form of array
          if ( docs && docs.length > 0 ) {
            res.status( 200 ).json(
              {
                message: "Successfully retrieved all the user's docs",
                data: docs
              } )
          } else {
            res.status( 200 ).json(
              {
                message: "No docs found for this user",
                data: []
              } )
          }
        }
      } );
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )


/**
 *  Save a new doc
 */
routes.post( '/save', ( req, res ) => {
  if ( req.user ) {
    if ( req.body.doc ) {

      const doc = new Template( req.body.doc );
      doc.user_id = req.user._id;
      doc.data_type = "doc";

      doc.save( err => {
        if ( err ) {
          if ( err.code == "11000" ) {
            res.status( 422 ).json( { message: "Document id already exists!!" } )
          } else {
            res.status( 404 ).json( { message: "Error saving doc" } )
          }
        } else {

          res.status( 200 ).json( { message: "doc Saved Successfully", data: { id: doc._id.toString() } } )
        }
      } )
    } else {
      res.status( 404 ).send( {
        message: "no doc data were sent"
      } )
    }
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )


/**
 *  Get a specific  doc
 */
routes.get( '/get/:doc_id', ( req, res ) => {
  if ( req.user ) {
    Template.findOne(
      {
        _id: new ObjectId( req.params.doc_id ),
        user_id: new ObjectId( req.user._id ),
        data_type: "doc"
      },
      ( err, doc ) => {
        if ( err ) {
          res.status( 404 ).json( { message: "Error getting docs" } )
        }

        // Note: returned docs will be in form of Object
        if ( doc ) {
          res.status( 200 ).json(
            {
              message: "Successfully retrieved the doc",
              data: doc
            } )
        } else {
          res.status( 404 ).json(
            {
              message: "No docs found for this user"
            } )
        }
      } );
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

/**
 *  Update an existing doc
 */
routes.put( '/update/:doc_id', ( req, res ) => {
  if ( req.user ) {

    if ( req.body.doc ) {
      Template.updateOne( { _id: ObjectId( req.params.doc_id ) }, req.body.doc,
        ( err ) => {
          if ( err ) {
            res.status( 404 ).send( {
              message: "An error occured updating the document please try again"
            } )
          }/* else if ( doc.nModified == 0 ) {
            res.status( 404 ).send( {
              message: "No templete have been updated"
            } )
          }*/ else {
            res.status( 200 ).send(
              { message: "Document Updated Successfully" }
            )
          }
        } )
    } else {
      res.status( 404 ).send( {
        message: "no doc data were sent"
      } )
    }
  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

/**
 * Delete one Doc
 */
routes.delete( "/delete/:doc_id", ( req, res ) => {
  if ( req.user ) {

    Template.deleteOne( { _id: ObjectId( req.params.doc_id ) },
      ( err, doc ) => {
        if ( err ) {
          res.status( 404 ).send( {
            message: "An error occured deleting the document please try again"
          } )
        } else if ( doc.deletedCount == 0 ) {
          res.status( 404 ).send( {
            message: "No document have been deleted"
          } )
        } else {
          // Successfully deleted
          res.status( 200 ).send(
            { message: "Document Deleted Successfully" }
          )
        }


      } )

  } else {
    res.status( 403 ).send( {
      message: "You must be logged in"
    } )
  }
} )

module.exports = routes;
